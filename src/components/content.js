import React, { useState, useEffect } from "react";

export const Content = (props) => {
  return (
    <div>
      <div class="container">
      <div class="tabs">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Earn Entries</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">My Entries</button>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="task-wrap">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="task-1 d-flex justify-content-center">
                                    <div class="icon">
                                        <i class="bi bi-telegram"></i>
                                    </div>
                                    <div class="text">
                                        <p>Join Telegram Group</p>
                                    </div>
                                    <div class="point">
                                        +5
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="task-1 d-flex justify-content-center">
                                    <div class="icon">
                                        <i class="bi bi-telegram"></i>
                                    </div>
                                    <div class="text">
                                        <p>Join Telegram Group</p>
                                    </div>
                                    <div class="point">
                                        +5
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="task-1 d-flex justify-content-center">
                                    <div class="icon">
                                        <i class="bi bi-telegram"></i>
                                    </div>
                                    <div class="text">
                                        <p>Join Telegram Group</p>
                                    </div>
                                    <div class="point">
                                        +5
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    ...
                </div>

              </div>
        </div>
      </div>
    </div>
  );
};
