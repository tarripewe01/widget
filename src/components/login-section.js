import React, { useState, useEffect } from "react";

export const LoginSection = (props) => {
  return (
    <div>
      <div class="container">
        <div class="login-section">
            <form>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Username</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                  {/* <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> */}
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    {/* <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> */}
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            
        </div>
        <div class="info">
            <p><span><i class="bi bi-lock-fill"></i></span> Please Login First!</p>
        </div>
      </div>
    </div>
  );
};
