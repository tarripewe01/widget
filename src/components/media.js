import React, { useState, useEffect } from "react";

export const Media = (props) => {
  return (
    <div>
      <div class="container">
        <div class="media">
          <div class="row">
            <div class="col-md-12">
            <iframe width="100%" height="500" src="https://www.youtube.com/embed/fFDAggAyE2Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
