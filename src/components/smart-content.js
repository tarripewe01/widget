import React, { useState, useEffect } from "react";

import axios from "axios";
import "./smart-content.css";
import $ from "jquery";

import { TaskItem } from "./task-item";

export const SmartContent = (props) => {

  const [post, setPost] = React.useState(null);
  const [email, setEmail] = React.useState("");
  const [name, setName] = React.useState("");
  const [test, setTest] = React.useState("message");
  const [dataLogin, setDataLogin] = React.useState("");

  const [localData, setLocalData] = React.useState("");

  const [taskList, setTaskList] = React.useState([]);

  const [isLogin, setIsLogin] = React.useState(localStorage.getItem("dataLogin")?JSON.parse(localStorage.getItem("dataLogin")).isLoggedIn:false);

  

  const baseURL = "https://petoverse.io/whitelist/api/users";
  const loginBaseURL = "https://petoverse.io/whitelist/api/loginuserswidget";
  const taskBaseURL = "https://petoverse.io/whitelist/api/socialListing";


  React.useEffect(() => {
    posdata();
    getdata();
    getTaskdata();
  }, []);

  function getTaskdata() {
    const config = {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        responseType: "json",
      };
    
    axios.get(taskBaseURL, config).then((response) => {
        
        console.log("respon get task list", response.data);

        const responseAPI = response.data;
        setTaskList(responseAPI.socialdata);
        console.log(responseAPI.socialdata);
    });

    console.log("panggil", taskList.socialdata);
  }

  function getdata() {
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      responseType: "json",
    };

    axios.get(baseURL, config).then((response) => {
      setPost(response.data);
      console.log("respon get", post);
    });
  }

  function posdata() {
    const params = new URLSearchParams();
    params.append("email", email);
    params.append("fname", name);

    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      responseType: "json",
    };

    axios.post(
        loginBaseURL,
        params,
        config
      )
      .then((response) => {
        console.log("respon post", response.data);
      });
  }

  function login(event) {
    event.preventDefault();

    const fname = $("#fname").val();
    setName(fname);

    const email = $("#email").val();
    setEmail(email);

    // alert(fname + " " + email);

    const params = new URLSearchParams();
    params.append("email", email);
    params.append("fname", fname);

    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      responseType: "json",
    };

    axios.post(
        loginBaseURL,
        params,
        config
      )
      .then((response) => {
        setDataLogin(response.data.datauser)
        console.log("respon post", response.data);
        window.localStorage.setItem("dataLogin", JSON.stringify(response.data.datauser));
        setLocalData(response.data.datauser);
        setIsLogin(true);
      });

      // const data = window.localStorage.getItem(dataLogin);
      

    //   alert(data.message);

    
  }

  return (
    <div id="background-smart-content">
      {/* <!-- The video --> */}
      <video autoPlay muted loop id="myVideo">
        <source src="../FlokiRushBG.mp4" type="video/mp4" />
      </video>

      <div className="container">
        <div className="head">
          <div className="row">
            <div className="col-4 info info-1">
              <div className="col-md-12">

                  test

                <h3>0</h3>
              </div>
              <div className="col-md-12 ">My Entries</div>
            </div>
            <div className="col-4 info info-2">
              <div className="col-md-12">
                <h3>10901</h3>
              </div>
              <div className="col-md-12">All Entries</div>
            </div>
            <div className="col-4 info info-3">
              <div className="col-md-12">
                <h3>1D</h3>
              </div>
              <div className="col-md-12">07:30:00</div>
            </div>
          </div>
        </div>
        <div className="media">
          <div className="row">
            <div className="col-md-12">
              <iframe
                width="100%"
                height="500"
                src="https://www.youtube.com/embed/fFDAggAyE2Y"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>

        <div className="text-title">
          <h1>Judul</h1>
        </div>
        <div className="text-deskripsi">
          <p>text-deskripsi</p>
        </div>

        {!isLogin?
          <div className="login-section">
          <form className="need-validation">
            <div className="mb-3">
              <label for="fullName" className="form-label">
                Full Name <span>*</span>
              </label>
              <input
                type="text"
                className="form-control"
                id="fname"
                aria-describedby="emailHelp"
                required
              />
              {/* <!-- <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> --> */}
            </div>
            <div className="mb-3">
              <label for="emailAddress" className="form-label">
                Email address <span>*</span>
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                aria-describedby="emailHelp"
                required
              />
              {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
            </div>
            <center>
              <button
                type="submit"
                className="btn btn-primary col-12"
                onClick={login}
              >
                Login
              </button>
            </center>
          </form>
          <div className="alert alert-warning info mt-3" role="alert">
            <i className="bi bi-lock-fill"></i> Please Login First! { test }
          </div>
        </div>

        :<h2>sudah login</h2>

        }
        

        

        <div className="task-content">
          <ul className="nav nav-tabs nav-justified" id="myTab" role="tablist">
            <li className="nav-item" role="presentation">
              <button
                className="nav-link active"
                id="home-tab"
                data-bs-toggle="tab"
                data-bs-target="#home"
                type="button"
                role="tab"
                aria-controls="home"
                aria-selected="true"
              >
                Earn Entries
              </button>
            </li>
            <li className="nav-item" role="presentation">
              <button
                className="nav-link"
                id="profile-tab"
                data-bs-toggle="tab"
                data-bs-target="#profile"
                type="button"
                role="tab"
                aria-controls="profile"
                aria-selected="false"
              >
                My Entries
              </button>
            </li>
          </ul>
          <div className="tab-content" id="myTabContent">
            <div
              className="tab-pane fade show active"
              id="home"
              role="tabpanel"
              aria-labelledby="home-tab"
            >
              <div className="task-wrap">
                <div className="col-md-12">
                  
                    
                    {/* {taskList.map(task => {
                        return task.social_med_name;
                    })} */}

                  
                  <TaskItem data = {taskList} />
                  
                </div>
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="profile"
              role="tabpanel"
              aria-labelledby="profile-tab"
            >
              <div className="card text-center mt-2 mb-2">
                <div className="card-header">Join Telegram Group</div>
                <div className="card-body">
                  <h5 className="card-title">@hgsavdjhsv</h5>
                  {/* <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" className="btn btn-primary">Go somewhere</a> */}
                </div>
                <div className="card-footer text-muted">
                  2 days ago (20/01/2022 12:00:00)
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="info">
          <div className="row">
            <div className="col-4">
              <div className="col-md-12">1</div>
              <div className="col-md-12">Winner</div>
            </div>
            <div className="col-4">
              <div className="col-md-12">01-21-2022</div>
              <div className="col-md-12">End Date</div>
            </div>
            <div className="col-4">
              <div className="col-md-12">Rules</div>
              <div className="col-md-12">Show</div>
            </div>
          </div>
        </div>

        <div className="footer">
          <footer>
            <div className="footer-content">
              <h3>Meta Floki Rush</h3>
              <p>
                Powered by <a href="#">SmartWidget</a>
              </p>

              {/* <div classNameName="flex-row title-header-soscial-icon">
                        <span classNameName="navbar-brand flex">
                        <a classNameName="navbar-brand" href="#">
                            <i classNameName="bi bi-facebook glow"></i>
                        </a>
                        </span>
                        <span classNameName="navbar-brand flex">
                        <a classNameName="navbar-brand glow" href="#">
                            <i classNameName="bi bi-instagram glow"></i>
                        </a>
                        </span>
                        <span classNameName="navbar-brand flex">
                        <a classNameName="navbar-brand" href="#">
                            <i classNameName="bi bi-telegram glow"></i>
                        </a>
                        </span>
                        <span classNameName="navbar-brand flex-grow-1">
                        <a classNameName="navbar-brand" href="#">
                            <i classNameName="bi bi-twitter glow"></i>
                        </a>
                        </span>
                    </div> */}
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
};
