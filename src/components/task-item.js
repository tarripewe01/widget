import React, { useState, useEffect } from "react";

export const TaskItem = (props) => {

  const data = props.data;
  console.log(data);

  return (
    <div>
      {data.length > 0 && data.map(task => {
        return (
          <div className="row">
            <div className="task">
              <div className="task-icon">
                <i className="bi bi-telegram"></i>
              </div>

              <div className="task-text">{task.social_med_name}</div>
              <div className="task-point">+5</div>
            </div>
          </div>
        );
    })}
      

    </div>
  );
};
